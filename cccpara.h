/* Created by text2h , (c) by Ulli Meybohm, www.meybohm.de   */
#ifndef cccpara_H
#define cccpara_H

char * cccparaV[]=
{
{"-h"},
{"--help"},
{"-t"},
{"--title"},
{"-s"},
{"--subtitle"},
{"-b"},
{"--backcovertext"},
{"-c"},
{"--count"},
{"-o"},
{"--outputfile"},
{"-l"},
{"--sidetext-left"},
{"-r"},
{"--sidetext-right"},
{""}
};

int cccparaC=16;


#endif
