CC=g++
INSTDIR=/usr/local/bin
CFLAGS=-Wall
STRINGLIB=bytevector.o stringlist.o

ccc: ccc.o
	$(CC) $(STRINGLIB) texmaker.o ccc.o -o cdcover $(CFLAGS)

ccc.o: ccc.cc texmaker.o stringlist.o bytevector.o 
	$(CC) -c ccc.cc $(CFLAGS)

texmaker.o: texmaker.h texmaker.cc stringlist.o bytevector.o
	$(CC) -c texmaker.cc $(CFLAGS)

headers: 
	text2h ccchelp <ccchelp.txt >ccchelp.h 
	text2h cccpara <cccpara.txt >cccpara.h
	text2h cccpara2 <cccpara2.txt >cccpara2.h
	text2h reshead <reshead.tex >reshead.h
	text2h resback <resback.tex >resback.h
	text2h resfront <resfront.tex >resfront.h

text2h: text2h.o bytevector.o stringlist.o 
	$(CC) $(STRINGLIB) text2h.o -o text2h $(CFLAGS)

text2h.o: text2h.cc
	$(CC) -c text2h.cc $(CFLAGS)

bytevector.o: bytevector.h bytevector.cc
	${CC} -c bytevector.cc $(CFLAGS)

stringlist.o: bytevector.o stringlist.h stringlist.cc
	$(CC) -c stringlist.cc $(CFLAGS)

clean:
	rm *.o cdcover

install: ccc
	cp cdcover $(INSTDIR)
