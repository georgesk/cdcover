/* Created by text2h , (c) by Ulli Meybohm, www.meybohm.de   */
#ifndef resfront_H
#define resfront_H

char * resfrontV[]=
{
{""},
{"%%%%%%%%%"},
{"% FRONT %"},
{"%%%%%%%%%"},
{""},
{"\\noindent"},
{"\\framebox{\\parbox[c][12.0cm][t]{12.0cm}"},
{"{"},
{"\\vspace{1cm}"},
{""},
{"\\noindent"},
{"\\begin{center}"},
{"{"},
{"\\parbox{10cm}{{\\Large\\color{black}%TITLE%}}"},
{""},
{"\\rule{10cm}{1pt}"},
{"\\vspace{0.1cm}"},
{""},
{"\\noindent"},
{"\\parbox{10cm}{{\\hfill\\large{\\color{black}%SUBTITLE%}}}"},
{"}"},
{"\\end{center}"},
{""},
{"\\vfill"},
{"\\begin{center}"},
{""},
{"\\parbox{10cm}{{\\footnotesize\\sffamily\\mylabel}}"},
{"\\end{center}"},
{""},
{"}}"},
{""},
{"%%%%%%%%%"},
{""},
{""},
{"\\vspace{1cm}"},
{""}
};

int resfrontC=35;


#endif
